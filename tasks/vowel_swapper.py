def vowel_swapper(string):
    # ==============
    # Your code here
    newstring = string.replace("a", "4")
    newstring = newstring.replace("A", "4")
    newstring = newstring.replace("e", "3")
    newstring = newstring.replace("E", "3")
    newstring = newstring.replace("i", "!")
    newstring = newstring.replace("I", "!")
    newstring = newstring.replace("o", "ooo")
    newstring = newstring.replace("O", "000")
    newstring = newstring.replace("u", "|_|")
    newstring = newstring.replace("U", "|_|")
    return newstring
    # ==============

print(vowel_swapper("aA eE iI oO uU")) # Should print "44 33 !! ooo000 |_||_|" to the console
print(vowel_swapper("Hello World")) # Should print "H3llooo Wooorld" to the console 
print(vowel_swapper("Everything's Available")) # Should print "3v3ryth!ng's 4v4!l4bl3" to the console
